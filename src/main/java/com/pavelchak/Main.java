package com.pavelchak;

import com.pavelchak.Controller.BattleController;
import com.pavelchak.Model.DroidBattle;
import com.pavelchak.View.UserView;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        DroidBattle droidBattle = new DroidBattle();
        BattleController battleController = new BattleController(droidBattle);
        UserView userView = new UserView(battleController);
        droidBattle.subscribe(userView);
        userView.show();
    }

}
