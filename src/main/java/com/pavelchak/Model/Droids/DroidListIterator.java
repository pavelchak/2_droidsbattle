package com.pavelchak.Model.Droids;

import java.util.List;

public class DroidListIterator {
    private List<BaseDroid> droidGroup;
    private int currentPosition = -1;


    public DroidListIterator(List<BaseDroid> droidGroup) {
        this.droidGroup = droidGroup;
    }

    public BaseDroid next() {
        currentPosition++;
        return droidGroup.get(currentPosition);
    }

    public boolean hasNext() {
        return (currentPosition < droidGroup.size() - 1) ? true : false;
    }

    public BaseDroid remove(BaseDroid droid) {
        droidGroup.remove(droid);
        if(currentPosition!=-1){
            currentPosition--;
        }
        return droid;
    }

    public BaseDroid remove() {
        BaseDroid droid = droidGroup.get(currentPosition);
        droidGroup.remove(droid);
        currentPosition--;
        return droid;
    }

    public int size() {
        return droidGroup.size();
    }

    public boolean isEmpty() {
        return (droidGroup.size() == 0) ? true : false;
    }
}
