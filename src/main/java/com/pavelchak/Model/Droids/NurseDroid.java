package com.pavelchak.Model.Droids;

import com.pavelchak.Model.Droids.Weapons.Doctor;

import java.io.Serializable;
import java.util.List;
import java.util.Random;

public class NurseDroid extends BaseDroid implements Serializable {
    public NurseDroid(String name, List<BaseDroid> myGroup, List<BaseDroid> enemyGroup) {
        super(name, myGroup, enemyGroup);
        //characteristics
        setMaxHealth(120);
        setHealth(120);
        setStrength(5);
        setAttack(70);
        setArmor(2);
        setDefence(40);
        setLuck(60);
        //weapon
        setWeapon1(new Doctor());
        setWeapon2(new Doctor());
    }
}
