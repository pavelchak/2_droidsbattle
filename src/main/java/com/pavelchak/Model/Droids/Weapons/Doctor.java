package com.pavelchak.Model.Droids.Weapons;

import com.pavelchak.Model.Droids.BaseDroid;

import java.util.Random;

public class Doctor extends BaseWeapon implements WeaponInterface {
    public Doctor() {
        setSelf(true);
        setShot(+10);
    }

    @Override
    public boolean kickSelfGroup() {
        return isSelf();
    }

    @Override
    public int kickDroid(BaseDroid attackingDroid, BaseDroid attackedDroid, Random random) {
        attackedDroid.setHealth(attackedDroid.getHealth() + getShot());
        if (attackedDroid.getHealth() > attackedDroid.getMaxHealth()) {
            attackedDroid.setHealth(attackedDroid.getMaxHealth());
        }
        return getShot();
    }
}
