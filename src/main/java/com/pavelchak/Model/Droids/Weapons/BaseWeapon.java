package com.pavelchak.Model.Droids.Weapons;

public class BaseWeapon {
    private boolean self;
    private int shot;

    public boolean isSelf() {
        return self;
    }

    public void setSelf(boolean self) {
        this.self = self;
    }

    public int getShot() {
        return shot;
    }

    public void setShot(int shot) {
        this.shot = shot;
    }
}
