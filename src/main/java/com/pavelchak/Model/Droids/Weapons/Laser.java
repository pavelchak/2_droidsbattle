package com.pavelchak.Model.Droids.Weapons;

import com.pavelchak.Model.Droids.BaseDroid;

import java.util.Random;

public class Laser extends BaseWeapon implements WeaponInterface {
    public Laser(){
        setSelf(false);
        setShot(-20);
    }

    @Override
    public boolean kickSelfGroup(){
        return isSelf();
    }

    @Override
    public int kickDroid(BaseDroid attackingDroid, BaseDroid attackedDroid, Random random) {
        int updateShot = getShot() + (int) (0.2 * getShot() * attackingDroid.getStrength());
        int damageForAttacked;
        if (random.nextInt(100) + attackingDroid.getLuck() - 100 > 50){
            damageForAttacked=updateShot;
        }else{
            damageForAttacked = updateShot + attackedDroid.getArmor();
            if(damageForAttacked>0){
                damageForAttacked=0;
            }
        }
        attackedDroid.setHealth(attackedDroid.getHealth()+damageForAttacked);

        if (attackedDroid.getHealth() < 0) {
            attackedDroid.setHealth(0);
        }
        return damageForAttacked;
    }


}
