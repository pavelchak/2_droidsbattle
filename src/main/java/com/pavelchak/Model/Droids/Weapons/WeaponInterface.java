package com.pavelchak.Model.Droids.Weapons;

import com.pavelchak.Model.Droids.BaseDroid;

import java.util.Random;

public interface WeaponInterface {
    int kickDroid(BaseDroid attackingDroid, BaseDroid attackedDroid, Random random);
    boolean kickSelfGroup();
}
