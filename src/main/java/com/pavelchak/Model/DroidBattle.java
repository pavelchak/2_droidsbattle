package com.pavelchak.Model;

import com.pavelchak.Exceptions.*;
import com.pavelchak.Model.DTO.ModelInfoDTO;
import com.pavelchak.Model.DTO.SampleBattle;
import com.pavelchak.Model.Droids.*;
import com.pavelchak.Model.ObserverInterface.Publisher;
import com.pavelchak.Model.ObserverInterface.Subscriber;

import java.util.*;

public class DroidBattle implements Publisher {
    final String[][] infoDroidsList
            = {{"Goblin", "Shotgun", "Blaster"},
            {"Nurse", "Doctor", "Doctor"},
            {"Paladin", "Shotgun", "Blaster"}};

    final int MAX_DROID_GROUP = 10;
    private List<BaseDroid> droidGroup1 = new LinkedList<>();
    private List<BaseDroid> droidGroup2 = new LinkedList<>();
    private CurrentBattle currentBattle = new CurrentBattle();
    private List<SampleBattle> historyBattle = new ArrayList<>();
    private List<SampleBattle> historyRound;
    private int round;

    private List<Subscriber> subscribers = new ArrayList<>();

    public class CurrentBattle {
        public List<SampleBattle> getHistoryRound() {
            return historyRound;
        }

        public int getRound() {
            return round;
        }
    }

    public DroidBattle() {

    }

    private BaseDroid createDroid(String classDroid, String name, List<BaseDroid> myGroup, List<BaseDroid> enemyGroup)
            throws NoSuchDroidException {
        if (classDroid == "Goblin") return new GoblinDroid(name, myGroup, enemyGroup);
        if (classDroid == "Nurse") return new NurseDroid(name, myGroup, enemyGroup);
        if (classDroid == "Paladin") return new PaladinDroid(name, myGroup, enemyGroup);
        throw new NoSuchDroidException();
    }

    public void setDroidGroup1(List<String> listDroids) throws NoSuchDroidException {
        droidGroup1.clear();
        for (String classDroid : listDroids) {
            String name = classDroid + "-1-" + (droidGroup1.size() + 1);
            droidGroup1.add(createDroid(classDroid, name, droidGroup1, droidGroup2));
        }
    }

    public void setDroidGroup2(List<String> listDroids) throws NoSuchDroidException {
        droidGroup2.clear();
        for (String classDroid : listDroids) {
            String name = classDroid + "-2-" + (droidGroup2.size() + 1);
            droidGroup2.add(createDroid(classDroid, name, droidGroup2, droidGroup1));
        }
    }

    private void startBattle() {
        DroidListIterator firstKickGroup;
        DroidListIterator secondKickGroup;
        historyBattle.clear();
        Random random = new Random();
        this.round = 1;
        do {
            historyRound = new ArrayList<>();

            if (random.nextBoolean()) {
                firstKickGroup = new DroidListIterator(droidGroup1);
                secondKickGroup = new DroidListIterator(droidGroup2);
            } else {
                firstKickGroup = new DroidListIterator(droidGroup2);
                secondKickGroup = new DroidListIterator(droidGroup1);
            }

            do {
                if (firstKickGroup.hasNext()) {
                    BaseDroid droid = firstKickGroup.next().addCurrentBattle(currentBattle).kick(random);
                    if (droid.getHealth() == 0) {
                        secondKickGroup.remove(droid);
                    }
                }
                if (secondKickGroup.hasNext()) {
                    BaseDroid droid = secondKickGroup.next().addCurrentBattle(currentBattle).kick(random);
                    if (droid.getHealth() == 0) {
                        firstKickGroup.remove(droid);
                    }
                }
            } while ((!firstKickGroup.isEmpty() && !secondKickGroup.isEmpty())      //There are no empty droid lists
                    && (firstKickGroup.hasNext() || secondKickGroup.hasNext()));    //any has nextDroid

            historyBattle.addAll(historyRound);
            publishInformAboutRound(historyRound);
            this.round++;
            delay(3000);
        } while (!firstKickGroup.isEmpty() && !secondKickGroup.isEmpty());
    }

    private void delay(int msec) {
        try {
            Thread.sleep(msec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void activateBattle() throws GroupsAreEmptyException {
        if (droidGroup1 == null || droidGroup2 == null) {
            throw new GroupsAreEmptyException();
        }
        publishInform("BEGIN DROIDS BATTLE ...................");
        startBattle();
        publishInform("END DROIDS BATTLE .....................");
        if (!droidGroup1.isEmpty()) {
            publishInform("The Droid Group 1 is WINNER. Saved droids:");
            for(BaseDroid droid : droidGroup1)
                System.out.println(droid.getNameDroid());
        } else {
            publishInform("The Droid Group 2 is WINNER. Saved droids:");
            for(BaseDroid droid : droidGroup2)
                System.out.println(droid.getNameDroid());
        }
    }

    @Override
    public void subscribe(Subscriber subscriber) {
        subscribers.add(subscriber);
    }

    @Override
    public void publishModelInfo() {
        if (subscribers.size() != 0) {
            ModelInfoDTO modelInfoDTO = new ModelInfoDTO();
            modelInfoDTO.setMaxDroidGroup(MAX_DROID_GROUP);
            modelInfoDTO.setInfoDroidsList(infoDroidsList);
            for (Subscriber subscriber : subscribers) {
                subscriber.update(modelInfoDTO);
            }
        }
    }

    private void publishInform(String message) {
        for (Subscriber subscriber : subscribers) {
            subscriber.inform(message);
        }
    }

    private void publishInformAboutRound(List<SampleBattle> historyRound) {
        for (Subscriber subscriber : subscribers) {
            subscriber.informAboutRound(historyRound);
        }
    }
}
