package com.pavelchak.Model;

public enum ModelStatus {
    OK_STATUS,
    ERROR_STATUS,
    NO_SUCH_DROID_STATUS,
    GROUPS_ARE_EMPTY_STATUS
}
