package com.pavelchak.Model.ObserverInterface;

public interface Publisher {
    void subscribe(Subscriber subscriber);
//    void unsubscribe(Subscriber subscriber);
    void publishModelInfo();
}
