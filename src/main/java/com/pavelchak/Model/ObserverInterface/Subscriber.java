package com.pavelchak.Model.ObserverInterface;

import com.pavelchak.Model.DTO.ModelInfoDTO;
import com.pavelchak.Model.DTO.SampleBattle;

import java.util.List;

public interface Subscriber {
    void update(ModelInfoDTO modelInfoDTO);
    void informAboutRound(List<SampleBattle> historyRound);
    void inform(String message);
}
