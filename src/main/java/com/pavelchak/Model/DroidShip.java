package com.pavelchak.Model;

import com.pavelchak.Model.Droids.BaseDroid;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DroidShip<T extends BaseDroid> implements Serializable {
    List<T> droidList;

    public DroidShip(){
        droidList=new ArrayList<>();
    }

    public List<T> get() {
        return droidList;
    }

    public void put(T droid){
        droidList.add(droid);
    }

    public void copyToBaseDroidShip(DroidShip<? super BaseDroid> baseDroidShip){
        for(BaseDroid droid: droidList){
            baseDroidShip.get().add(droid);
        }

    }

}
