package com.pavelchak.Model.DTO;

public class ModelInfoDTO {
    private int maxDroidGroup;
    private String[][] infoDroidsList;

    public int getMaxDroidGroup() {
        return maxDroidGroup;
    }
    public void setMaxDroidGroup(int maxDroidGroup) {
        this.maxDroidGroup = maxDroidGroup;
    }

    public String[][] getInfoDroidsList() {
        return infoDroidsList;
    }
    public void setInfoDroidsList(String[][] infoDroidsList) {
        this.infoDroidsList = infoDroidsList;
    }
}
