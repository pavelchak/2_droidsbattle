package com.pavelchak.Model.DTO;

import com.pavelchak.Model.Droids.BaseDroid;

public class DroidDTO {
    private String nameDroid;

    private int health;         //point
    private int strength;       //point
    private int armor;          //point
    private int luck;           //1-100%

    public DroidDTO(BaseDroid droid) {
        this.nameDroid=droid.getNameDroid();
        this.health = droid.getHealth();
        this.strength = droid.getStrength();
        this.armor = droid.getArmor();
        this.luck = droid.getLuck();
    }

    public int getHealth() {
        return health;
    }

    @Override
    public String toString() {
        return nameDroid + "(H:" + health +" A:"+armor+ ")";
    }

    public String getNameDroid() {
        return nameDroid;
    }

    public int getStrength() {
        return strength;
    }

    public int getArmor() {
        return armor;
    }

    public int getLuck() {
        return luck;
    }
}
