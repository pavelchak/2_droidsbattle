package com.pavelchak.Model.DTO;

import com.pavelchak.Model.Droids.BaseDroid;

public class SampleBattle {
    private int round;
    private DroidDTO attackingDroid;
    private DroidDTO attackedDroid;
    private int shot;

    public SampleBattle(int round, BaseDroid attackingDroid, BaseDroid attackedDroid, int shot) {
        this.round = round;
        this.attackingDroid = new DroidDTO(attackingDroid);
        this.attackedDroid = new DroidDTO(attackedDroid);
        this.shot = shot;
    }

    @Override
    public String toString() {
        String sign = shot > 0 ? "+" : "";
        return "Round:" + round + " " + attackingDroid.toString() + " kicks " + sign + shot + " " + attackedDroid.toString();
    }

    public int getRound() {
        return round;
    }

    public DroidDTO getAttackingDroid() {
        return attackingDroid;
    }

    public DroidDTO getAttackedDroid() {
        return attackedDroid;
    }

    public int getShot() {
        return shot;
    }
}
