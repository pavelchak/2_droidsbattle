package com.pavelchak.View;

import com.pavelchak.Controller.BattleController;
import com.pavelchak.Model.DTO.ModelInfoDTO;
import com.pavelchak.Model.DTO.SampleBattle;
import com.pavelchak.Model.DroidShip;
import com.pavelchak.Model.Droids.BaseDroid;
import com.pavelchak.Model.Droids.GoblinDroid;
import com.pavelchak.Model.Droids.NurseDroid;
import com.pavelchak.Model.ObserverInterface.Subscriber;

import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.List;

public final class UserView implements Subscriber {
    BattleController battleController;

    private static Scanner input = new Scanner(System.in);
    BufferedReader charScanner = new BufferedReader(new InputStreamReader(System.in));
    private Map<String, String> menu;

    private String[][] infoDroidsList;
    private int groupAmount = 0;
    private ModelInfoDTO modelInfoDTO;
    List<String> droidGroup1 = null;
    List<String> droidGroup2 = null;

    public UserView(BattleController battleController) {
        menu = new LinkedHashMap<>();
        menu.put("I", "  I - info about game");
        menu.put("S", "  S - select groups");
        menu.put("G", "  G - info about groups");
        menu.put("R", "  R - run battle!");
        menu.put("T", "  T - test ship (generics)");
        menu.put("1", "  1 - Serialize ship");

        menu.put("Q", "  Q - exit game");
        this.battleController = battleController;
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values())
            System.out.println(str);
    }

    private int inputIndexDroid(int maxDiffDroids) {
        int droidIndex;
        do {
            try {
                droidIndex = input.nextInt();
                if (droidIndex >= maxDiffDroids) {
                    throw new Exception();
                }
            } catch (Exception e) {
                input.nextLine();
                droidIndex = -1;
                System.out.println("Invalid droid number. Input again.");
            }
        } while (droidIndex == -1);
        return droidIndex;
    }

    private void outputSubMenuSelectGroup() throws IOException {
        if (modelInfoDTO == null) {
            System.out.println("Model information is not available");
            charScanner.readLine();
            return;
        }
        droidGroup1 = new ArrayList<>();
        droidGroup2 = new ArrayList<>();
        do {
            System.out.println("Select amount group 1..10");
            try {
                groupAmount = input.nextInt();
            } catch (Exception e) {
                input.nextLine();
                groupAmount = -1;
            }
        } while (groupAmount < 1 || groupAmount > modelInfoDTO.getMaxDroidGroup());

        System.out.println("DROID MODEL LIST");
        for (int i = 0; i < modelInfoDTO.getInfoDroidsList().length; i++) {
            String info = "   " + i + " - " + modelInfoDTO.getInfoDroidsList()[i][0] + "{" +
                    modelInfoDTO.getInfoDroidsList()[i][1] + ", " + modelInfoDTO.getInfoDroidsList()[i][2] + "}";
            System.out.println(info);
        }
        System.out.println("--------------------------------");
        System.out.println("Select droid models for Group1 (input number of models, example 0 1 1 0 2):");
        for (int i = 0; i < groupAmount; i++) {
            int droidIndex = inputIndexDroid(modelInfoDTO.getInfoDroidsList().length);
            droidGroup1.add(modelInfoDTO.getInfoDroidsList()[droidIndex][0]);
        }
        input.nextLine();
        System.out.println("--------------------------------");
        System.out.println("Select droid models for Group2 (input number of models):");
        for (int i = 0; i < groupAmount; i++) {
            int droidIndex = inputIndexDroid(modelInfoDTO.getInfoDroidsList().length);
            droidGroup2.add(modelInfoDTO.getInfoDroidsList()[droidIndex][0]);
        }
    }

    private void readGameInfo() throws IOException {
        Scanner file = new Scanner(new File("gameinfo.txt"));
        while (file.hasNextLine()) {
            System.out.println(file.nextLine());
        }
        file.close();
        charScanner.readLine();
    }

    private void readGroupInfo() throws IOException {
        if (droidGroup1 == null || droidGroup2 == null) {
            System.out.println("First, enter the groups!");
            charScanner.readLine();
            return;
        }
        System.out.println("GROUP1:");
        for (String str : droidGroup1) {
            System.out.println("  " + str);
        }
        System.out.println("---------------");
        System.out.println("GROUP2:");
        for (String str : droidGroup2) {
            System.out.println("  " + str);
        }
        System.out.println("---------------");
        charScanner.readLine();

    }

    private void runBattle() throws IOException {
        if (droidGroup1 == null || droidGroup2 == null) {
            System.out.println("First, enter the groups!");
            charScanner.readLine();
            return;
        }
        battleController.sendGroupsToModel(droidGroup1, droidGroup2);
        battleController.startBattle();
        charScanner.readLine();
    }

    public void show() throws IOException, ClassNotFoundException {
        battleController.getModelInfo();
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.next().toUpperCase();
            switch (keyMenu) {
                case "I":
                    readGameInfo();
                    break;
                case "G":
                    readGroupInfo();
                    break;
                case "S":
                    outputSubMenuSelectGroup();
                    break;
                case "R":
                    runBattle();
                    break;
                case "T":
                    testShipGenerics();
                    break;
                case "1":
                    testSerializeShip();
                    break;

            }
        } while (!keyMenu.equals("Q"));
    }

    public void testShipGenerics(){
        // Test Generics class DroidShip
        System.out.println("\nAdd Andrii and Vasia to DroidShip<GoblinDroid> -------");
        DroidShip<GoblinDroid> droidShip = new DroidShip<>();
        droidShip.put(new GoblinDroid("Andrii", null, null));
        droidShip.put(new GoblinDroid("Vasia", null, null));
        for (BaseDroid droid : droidShip.get()) {
            System.out.println(droid);
        }
        System.out.println("\nAdd Maria and Ania to DroidShip<NurseDroid> -------");
        DroidShip<NurseDroid> droidShip2 = new DroidShip<>();
        droidShip2.put(new NurseDroid("Maria", null, null));
        droidShip2.put(new NurseDroid("Ania", null, null));
        for (BaseDroid droid : droidShip2.get()) {
            System.out.println(droid);
        }
        System.out.println("\nAdd DroidShip<GoblinDroid> and DroidShip<NurseDroid> to DroidShip<BaseDroid>");
        DroidShip<BaseDroid> droidShip3 = new DroidShip<>();
        droidShip.copyToBaseDroidShip(droidShip3);
        droidShip2.copyToBaseDroidShip(droidShip3);
        for (BaseDroid droid : droidShip3.get()) {
            System.out.println(droid);
        }
    }

    public void testSerializeShip() throws IOException, ClassNotFoundException {
        // Test Generics class DroidShip
        System.out.println("\nAdd Andrii and Vasia to DroidShip<GoblinDroid> -------");
        DroidShip<GoblinDroid> droidShip = new DroidShip<>();
        droidShip.put(new GoblinDroid("Andrii", null, null));
        droidShip.put(new GoblinDroid("Vasia", null, null));
        for (BaseDroid droid : droidShip.get()) {
            System.out.println(droid);
        }

        System.out.println("\nSerializeShip");
        ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream("droids.dat"));
        out.writeObject(droidShip);
        out.close();

        System.out.println("\nDeserializeShip");
        ObjectInputStream in= new ObjectInputStream(
                new FileInputStream("droids.dat"));
        DroidShip<GoblinDroid> droidFromFile = (DroidShip)in.readObject();
        in.close();
        for (BaseDroid droid : droidFromFile.get()) {
            System.out.println(droid);
        }


    }
    @Override
    public void update(ModelInfoDTO modelInfoDTO) {
        this.modelInfoDTO = modelInfoDTO;
    }

    @Override
    public void informAboutRound(List<SampleBattle> historyRound) {
        for (SampleBattle sampleBattle : historyRound) {
            System.out.println(sampleBattle);
            Toolkit.getDefaultToolkit().beep();
        }
        System.out.println("\n");
    }

    @Override
    public void inform(String message) {
        System.out.println(message);
    }
}
