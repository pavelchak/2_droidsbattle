package com.pavelchak.Controller;

import com.pavelchak.Exceptions.GroupsAreEmptyException;
import com.pavelchak.Exceptions.NoSuchDroidException;
import com.pavelchak.Model.DroidBattle;
import com.pavelchak.Model.ModelStatus;

import java.util.List;

public class BattleController {
    public DroidBattle droidBattle;

    public BattleController(DroidBattle droidBattle) {
        this.droidBattle = droidBattle;
    }

    public void getModelInfo(){
        droidBattle.publishModelInfo();
    }

    public ModelStatus sendGroupsToModel(List<String> droidGroup1, List<String> droidGroup2) {
        try {
            droidBattle.setDroidGroup1(droidGroup1);
            droidBattle.setDroidGroup2(droidGroup2);
            return ModelStatus.OK_STATUS;
        } catch (NoSuchDroidException e) {
            return ModelStatus.NO_SUCH_DROID_STATUS;
        }
    }

    public ModelStatus startBattle() {
        try {
            droidBattle.activateBattle();
            return ModelStatus.OK_STATUS;
        } catch (GroupsAreEmptyException e) {
            return ModelStatus.GROUPS_ARE_EMPTY_STATUS;
        }
    }
}
